# Silent Install

The SilentInstall is an automate self-update application. It is installed as system application in rooted device to have install_package priviledge.
On new version available, the application downloads the new version apk, install and open it. 

### How to install system application
Open terminal and enable read/write  
```
    Adb shell
    Su
    mount -o remount,rw /system
 ```
Open another terminal and push the apk into system/priv-app location 
```
    adb push {apk-path} system/priv-app
```

Use ES File Explorer to locate system/priv-app/main.apk and install the application