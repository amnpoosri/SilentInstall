package com.example.codemate.silentinstall.service

import android.app.IntentService
import android.content.Intent
import android.net.Uri
import android.support.v4.content.LocalBroadcastManager
import android.util.Log
import android.widget.Toast
import com.example.codemate.silentinstall.model.InstallDao
import java.io.*


class InstallService : IntentService("Install Service") {

    private var mPath: String? = null

    override fun onHandleIntent(intent: Intent?) {
        mPath = intent!!.extras!!.getString(PATH)
        initInstall()
    }

    private fun initInstall() {
        Log.d("downloading", "install path: $mPath")
        try {
            if (!isRooted()) {
                //if your device is not rooted
                val intent_install = Intent(Intent.ACTION_VIEW)
                intent_install.flags = Intent.FLAG_ACTIVITY_NEW_TASK
                val file = File(mPath)
                file.setReadable(true, false);
                intent_install.setDataAndType(Uri.fromFile(file), "application/vnd.android.package-archive")
                startActivity(intent_install)
                Toast.makeText(applicationContext, "App Installing", Toast.LENGTH_LONG).show()
            } else {
//                Commands is used when the application is signed with device manufacturer key
//                val commands = arrayOf("su", "-c", command)
                val command = "pm install -r " + mPath!!
                Log.d("Installing Command:", command)
                val process: Process = Runtime.getRuntime().exec(command)
                process.waitFor()
            }

            // CallBack
            val dao = InstallDao()
            dao.isInstalled = true
            sendIntent(dao)

        } catch (e: Exception) {
            Log.d("downloading", "install error")
            e.printStackTrace()

            // CallBack
            val dao = InstallDao()
            dao.isInstalled = false
            sendIntent(dao)
        }

    }

    private fun isRooted(): Boolean {
        return findBinary("su")
    }

    private fun findBinary(binaryName: String): Boolean {
        var found = false
        if (!found) {
            val places = arrayOf("/sbin/", "/system/bin/", "/system/xbin/", "/data/local/xbin/", "/data/local/bin/", "/system/sd/xbin/", "/system/bin/failsafe/", "/data/local/")
            for (where in places) {
                if (File(where + binaryName).exists()) {
                    found = true
                    break
                }
            }
        }
        return found
    }


    private fun sendIntent(dao: InstallDao) {
        // Application Installed
        val intent = Intent(INSTALLING_PROGRESS)
        intent.putExtra(INSTALL, dao)
        LocalBroadcastManager.getInstance(this@InstallService).sendBroadcast(intent)
    }

    companion object {

        val INSTALLING_PROGRESS = "installing_progress"
        val INSTALL = "install"
        val PATH = "path"
    }

}