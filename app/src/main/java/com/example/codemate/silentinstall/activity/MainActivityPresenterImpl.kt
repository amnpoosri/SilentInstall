package com.example.codemate.silentinstall.activity

import android.util.Log
import com.example.codemate.silentinstall.R
import com.example.codemate.silentinstall.manager.HttpManager
import com.example.codemate.silentinstall.model.DownloadDao
import com.example.codemate.silentinstall.model.InstallDao
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import java.io.File

class MainActivityPresenterImpl(private val view: IMainActivity.View) : IMainActivity.Presenter {
    private val TAG = "MainActivityPresenterIm"
    private val Application = "Application"

    override fun checkForUpdates() {
        view.showProgressBar()
        HttpManager.getInstance().service.requestForceUpdate()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ response ->
                    Log.d(TAG , "asdasd : $response")
                    view.hideProgressBar()
                    if(response != null) {
//                        response.isForceUpdate = true
                        if (response!!.isForceUpdate) {
                            try {
                                val manager = view.getActivity().packageManager
                                val info = manager.getPackageInfo(view.getActivity().packageName, 0)
//                                response.androidVersionCode = 2
                                if (info.versionCode < response!!.androidVersionCode) {
                                    downloadApplication(response!!.appLink, Application)
                                }
                            } catch (e: Exception) {
                                // PackageManager fail to get versionCode.
                                e.printStackTrace()
                                showSimpleErrorMessage("Something went wrong, Please try again later.")
                            }
                        }
                    }
                }, { error ->
                    Log.d(TAG, "$error")
                    view.hideProgressBar()
                    showSimpleErrorMessage("Something went wrong, Please try again later.")
                })
    }

    override fun onDownloadReceiver(dao: DownloadDao) {
        try {
            if (dao.totalBytes != 0)
                view.updateProgress(((dao.soFarBytes.toFloat()/dao.totalBytes)*100.0).toInt())

            if (dao.progress === 100) {
                // Download complete.
                view.updateProgress(dao.progress)
                view.unSubscribeDownloading()

                if (dao.isFailure) {
                    // Download Failure
                    if (dao.fileName == Application)
                        showSimpleErrorMessage("Something went wrong, Please try again later.")
                } else {
                    // Download success and Being Install.
                    if (dao.fileName == Application) {
                        // Application Downloading
                        view.setForceUpdateMessage("Installing Application...")
                        installApplicationAPK(dao)

                    }
                }
            } else {
                // Downloading.
                if (dao.fileName == Application) {
                    // Application Downloading
                    view.setForceUpdateMessage("Downloading...")
                }
            }
        } catch (e: Exception) {
            e.printStackTrace()
        }

    }

    override fun onApplicationInstallReceiver(dao: InstallDao) {
        if (dao.isInstalled) {
            // Install complete.
            view.setForceUpdateMessage("Install completed.")
        } else {
            // Install failure.
            showSimpleErrorMessage("Something went wrong, Please try again later.")
            view.setButtonMessage(R.string.retry)
        }

        view.unSubscribeApplicationInstall()
    }

    override fun onInstallClicked() {
        view.hideInStallButton()
        view.setForceUpdateMessage("")
        checkForUpdates()
    }

    private fun showSimpleErrorMessage(msg: String) {
        view.setForceUpdateMessage(msg)
        view.setButtonMessage(R.string.retry)
    }


    private fun downloadApplication(link: String, name: String) {
        // Setup views for support downloadApplication here.
        view.hideInStallButton()

        // Start and subscribe Download service.
        view.subscribeDownloading()
        view.startDownloadService(link, name)
    }

    private fun installApplicationAPK(dao: DownloadDao) {
        val file = File(dao.filePath)
        if (file.exists()) {
            view.subscribeApplicationInstall()
            view.startInstallService(dao.filePath!!)
        } else {
            view.toastMessage("File not found.")
            showSimpleErrorMessage("Something went wrong, Please try again later.")
        }
    }

    override fun viewOnCreate() {

    }

    override fun viewOnStart() {
    }

    override fun viewOnResume() {

    }

    override fun viewOnPause() {

    }

    override fun viewOnStop() {

    }

    override fun viewOnDestroy() {
        view.clearAppDownloadData()
    }

    override fun viewOnCreateView() {

    }

    override fun viewOnDestroyView() {

    }
}
