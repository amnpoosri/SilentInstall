package com.example.codemate.silentinstall.activity

import android.app.Activity
import com.example.codemate.silentinstall.base.BasePresenter
import com.example.codemate.silentinstall.model.DownloadDao
import com.example.codemate.silentinstall.model.InstallDao

interface IMainActivity {

    interface View {
        fun getActivity(): Activity
        fun getForceUpdateMessage(): String
        fun toastMessage(msg: String)
        fun hideProgressBar()
        fun showProgressBar()
        fun setForceUpdateMessage(descMaintain: String)
        fun setButtonMessage(res: Int)
        fun showInStallButton()
        fun hideInStallButton()
        fun subscribeDownloading()
        fun unSubscribeDownloading()
        fun startDownloadService(url: String, fileName: String)
        fun updateProgress(progress: Int)
        fun startInstallService(filePath: String)
        fun subscribeApplicationInstall()
        fun unSubscribeApplicationInstall()
        fun clearAppDownloadData()
    }

    interface Presenter: BasePresenter {
        fun checkForUpdates()
        fun onDownloadReceiver(dao: DownloadDao)
        fun onApplicationInstallReceiver(dao: InstallDao)
        fun onInstallClicked()
    }

}
