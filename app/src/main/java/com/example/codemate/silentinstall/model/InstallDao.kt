package com.example.codemate.silentinstall.model

import android.os.Parcel
import android.os.Parcelable

class InstallDao : Parcelable {

    var isInstalled: Boolean = false

    constructor() {}

    protected constructor(`in`: Parcel) {
        isInstalled = `in`.readByte().toInt() != 0
    }

    override fun writeToParcel(dest: Parcel, flags: Int) {
        dest.writeByte((if (isInstalled) 1 else 0).toByte())
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object {

        val CREATOR: Parcelable.Creator<InstallDao> = object : Parcelable.Creator<InstallDao> {
            override fun createFromParcel(`in`: Parcel): InstallDao {
                return InstallDao(`in`)
            }

            override fun newArray(size: Int): Array<InstallDao?> {
                return arrayOfNulls(size)
            }
        }
    }
}
