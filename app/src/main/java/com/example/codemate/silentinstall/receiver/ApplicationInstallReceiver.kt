package com.example.codemate.silentinstall.receiver

import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.support.v4.content.LocalBroadcastManager
import android.util.Log
import android.widget.Toast

class ApplicationInstallReceiver : BroadcastReceiver() {

    override fun onReceive(context: Context, intent: Intent?) {
        if (intent == null || intent.data == null) {
            return
        }

        // Check application installed.
        val installedPackageName = intent.data!!.schemeSpecificPart
        Log.d("ApplicationInstalled", "ApplicationInstalled: $installedPackageName")
        Toast.makeText(context, "installed: $installedPackageName", Toast.LENGTH_LONG)
    }

}